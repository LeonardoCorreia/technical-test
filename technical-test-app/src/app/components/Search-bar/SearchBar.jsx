import React, { useState, useRef, useEffect } from "react";
import configuration from "./../../../configuration/config.json";

import "./SearchBar.scss";

let timer = null;

export default function SearchBar(props) {
  const [isAutocompleteVisible, setAutocompleteVisible] = useState(false);
  const [suggestionsList, setSuggestionsList] = useState([]);
  const [searchModel, setSearchModel] = useState("");
  const wrapperRef = useRef(null);

  const [errorMessage, setErrorMessage] = useState("");

  const searchBarRender = () => (
    <div>
      {errorMessage}
      <div ref={wrapperRef} className="container">
        {inputSearchRender()}
      
        {isAutocompleteVisible ? autocompleteRender() : false}
      </div>      
    </div>
  );

  const inputSearchRender = () => (
    <>
      <span aria-hidden="true" className="fa fa-search search-icon"></span>
      <input
        className={
          "search-bar" + (isAutocompleteVisible ? " autocomplete" : "")
        }
        type="text"
        placeholder="Search"
        onChange={(e) => onChangeInput(e.target.value)}
        onClick={() => onClickInputSearch()}
        value={searchModel}
        autoFocus={true}
      ></input>
    </>
  );

  const autocompleteRender = () => (
    <div className="autocomplete-container">
      <ul className="autocomplete-ul">
        {suggestionsList.map((suggestion, i) => (
          <li key={i} onClick={() => onSelectSuggestion(suggestion)}>
            <div>
              <strong>{suggestion["1. symbol"]}</strong>
              <div>{suggestion["2. name"]}</div>
            </div>
          </li>
        ))}
      </ul>
    </div>
  );

  useEffect(() => {
    document.addEventListener("mousedown", handleClickOutside);
    return () => {
      document.removeEventListener("mousedown", handleClickOutside);
    };
  }, []);

  const handleClickOutside = (event) => {
    const { current: wrap } = wrapperRef;
    if (wrap && !wrap.contains(event.target)) setAutocompleteVisible(false);
  };

  const fetchAutocomplete = async (value) => {
    let url = configuration.autocompleteBaseURL;
    url = `${url}&keywords=${value}`;
    url = `${url}&apikey=${configuration.apiKey}`;

    const response = await fetch(url);
    const data = await response.json();

    const hasData = data.bestMatches?.length > 0;

    setAutocompleteVisible(hasData);

    if (!hasData) return setErrorMessage(data[Object.keys(data)[0]]);

    setErrorMessage(false);

    setSuggestionsList(data.bestMatches);
  };

  const onChangeInput = (searchText) => {
    setSearchModel(searchText);

    clearTimeout(timer);
    timer = setTimeout(() => {
      if (searchText) fetchAutocomplete(searchText);
    }, 400);
  };

  const onClickInputSearch = () => {
    if (searchModel && !isAutocompleteVisible) fetchAutocomplete(searchModel);
  };

  const onSelectSuggestion = (suggestion) => {
    setSearchModel(suggestion["2. name"]);
    setAutocompleteVisible(false);

    props.onSelectOption(suggestion);
  };

  return searchBarRender();
}
