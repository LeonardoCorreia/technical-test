import React from "react";
import "./TabFooter.scss";

export default function TabFooter(props) {
  const refreshButtonRender = () => (
    <span className="refresh-table" onClick={() => refreshDetails()}>
      Refresh
      <span className="fa fa-retweet refresh-icon" />
    </span>
  );

  const refreshDetails = () => {
    props.refreshFooter();
  };

  return (
    <div>
      {refreshButtonRender()}
      {props.children}
    </div>
  );
}
