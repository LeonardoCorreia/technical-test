import React from "react";
import "./TabView.scss";

export default function TabView(props) {
  return (
    <>
      <div className="header-container">{props.children[0]}</div>
      <div className="footer-container">{props.children[1]}</div>
    </>
  );
}
