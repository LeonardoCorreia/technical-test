/* eslint-disable react-hooks/exhaustive-deps */
import React, { useState, useEffect } from "react";
import "./TabHeaderList.scss";
import "font-awesome/css/font-awesome.min.css";

export default function TabHeaderList(props) {
  const [isActiveKey, setIsActiveKey] = useState(0);

  const onSelectTab = (key) => {
    setIsActiveKey(key);
    props.onSelectTab(key);
  };

  useEffect(() => {
    setIsActiveKey(props.model.findIndex((x) => x === props.currentOption));
  }, [props.currentOption, props.model.length]);

  const tabButtonRender = (name, key, closeButtonRender) => (
    <button
      className={"header" + (isActiveKey === key ? " active" : "")}
      onClick={() => onSelectTab(key)}
      key={key}
    >
      {name}
      {closeButtonRender(key)}
    </button>
  );

  const closeButtonRender = (key) => (
    <span
      className="fa fa-times close-button"
      onClick={(e) => {
        e.stopPropagation();
        props.onCloseTab(key);
      }}
    />
  );

  return props.model.map((name, key) => tabButtonRender(name, key, closeButtonRender));
}
