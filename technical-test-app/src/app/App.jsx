import React from "react";
import "./App.scss";
import SearchLayout from "./pages/Search/SearchLayout";

export default function App() {
  return (
    <div className="App-header">
      <SearchLayout />
    </div>
  );
}
