import React, { useState } from "react";
import SearchBar from "../../components/Search-bar/SearchBar";
import TabView from "../../components/Tab-view/TabView";
import TabHeaderList from "../../components/Tab-view/TabHeader/TabHeaderList";
import TabFooter from "../../components/Tab-view/TabFooter/TabFooter";
import configuration from "./../../../configuration/config.json";

import "./SearchLayout.scss";

export default function SearchLayout() {
  const [optionSelectedList, setOptionSelectedList] = useState([]);
  const [currentOption, setCurrentOption] = useState({});

  const [selectedDetailsList, setSelectedDetailsList] = useState([]);
  const [currentDetail, setCurrentDetail] = useState({});

  const [errorMessage, setErrorMessage] = useState("");

  const searchLayout = () => (
    <div className="search-container">
      <SearchBar onSelectOption={onSelectOptionCallback} />
      <TabView>
        {tabHeaderListRender()}
        {currentTabFooterRender()}
      </TabView>
    </div>
  );

  const errorRender = () =>
    errorMessage ? <div className="error-message">{errorMessage}</div> : false;

  const tabHeaderListRender = () => (
    <TabHeaderList
      model={optionSelectedList.map((item) => item["2. name"])}
      currentOption={currentOption["2. name"]}
      onSelectTab={onSelectTab}
      onCloseTab={onCloseTab}
    ></TabHeaderList>
  );

  const currentTabFooterRender = () =>
    optionSelectedList.map((option, key) =>
      option === currentOption ? errorRender() || tabFooterRender(key) : false
    );

  const tabFooterRender = (key) => (
    <TabFooter key={key} refreshFooter={refreshDetails}>
      <table>
        <tbody>
          {Object.keys(currentDetail || {}).map((objectKey, index) => (
            <tr key={index}>
              <th>{objectKey}</th>
              <td>{currentDetail[objectKey]}</td>
            </tr>
          ))}
        </tbody>
      </table>
    </TabFooter>
  );

  const refreshDetails = () => {
    fetchDetails(currentOption["1. symbol"]);
  };

  const fetchDetails = async (value) => {
    let url = configuration.globalQuoteBaseURL;
    url = `${url}&symbol=${value}`;
    url = `${url}&apikey=${configuration.apiKey}`;

    const response = await fetch(url);
    const data = await response.json();

    if (!data["Global Quote"])
      return setErrorMessage(data[Object.keys(data)[0]]);

    setErrorMessage(false);

    const key = optionSelectedList.findIndex((x) => x["1. symbol"] === value);

    if (key === -1) {
      setSelectedDetailsList([...selectedDetailsList, data["Global Quote"]]);
      setCurrentDetail(data["Global Quote"]);
      return;
    }

    let update = selectedDetailsList.map((x, i) =>
      i === key ? data["Global Quote"] : x
    );

    setSelectedDetailsList(update);
  };

  const onSelectOptionCallback = (value) => {
    const key = optionSelectedList.findIndex(
      (x) => x["1. symbol"] === value["1. symbol"]
    );

    if (key === -1) {
      setOptionSelectedList([...optionSelectedList, value]);
      setCurrentOption(value);
      fetchDetails(value["1. symbol"]);
      return;
    }

    onSelectTab(key);
  };

  const onSelectTab = (key) => {
    setCurrentOption(optionSelectedList[key]);
    setCurrentDetail(selectedDetailsList[key]);
  };

  const onCloseTab = (key) => {
    let keyOfCurrent = optionSelectedList.indexOf(currentOption);

    setOptionSelectedList(
      optionSelectedList.filter((x) => optionSelectedList.indexOf(x) !== key)
    );

    setSelectedDetailsList(
      selectedDetailsList.filter((x) => selectedDetailsList.indexOf(x) !== key)
    );

    if (key !== keyOfCurrent) return;

    const newCurrent = keyOfCurrent > 0 ? keyOfCurrent - 1 : keyOfCurrent + 1;

    onSelectTab(newCurrent % optionSelectedList.length);
  };

  return searchLayout();
}
